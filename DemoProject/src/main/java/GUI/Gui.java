package GUI;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
public class Gui {

	public Gui() {
		// TODO Auto-generated method stub
			
			//////////////////////////////////////////////
			
			JFrame frame = new JFrame ("Fereastra");
			 frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			 frame.setSize(300, 210);
			 
			 
			 JPanel panel1 = new JPanel();
			 JPanel panel2 = new JPanel();
			 JPanel panel3 = new JPanel();
			 JPanel panel4 = new JPanel();
			 JPanel panel5 = new JPanel();
			 JPanel panel6 = new JPanel();
			


			 JButton b1 = new JButton("Adunare");
			 JButton b2 = new JButton("Scadere");
			 JButton b3 = new JButton("Inmultire");
			 JButton b4 = new JButton("Impartire");
			 JButton b5 = new JButton("Derivare");
			 JButton b6 = new JButton("Integrare");
			 
			 
			 panel1.add(b1);
			 panel2.add(b2);
			 panel3.add(b3);
			 panel4.add(b4);
			 panel5.add(b5);
			 panel6.add(b6);
		
			 
			 JPanel p = new JPanel();		 
			 p.add(panel1);
			 p.add(panel2);
			 p.add(panel3);
			 p.add(panel4);
			 p.add(panel5);
			 p.add(panel6);
			 
			 frame.setContentPane(p);
			 frame.setVisible(true);


b1.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent e) {
		 JFrame frame1= new JFrame("Adunare");
		 frame1.setSize(300,210);
		 JPanel panel1 = new JPanel();
		 JLabel label1 = new JLabel ("Polinomul 1:");
		 JTextField tf1 = new JTextField("Introduceti polinomul1");
		 panel1.add(label1);
		 panel1.add(tf1);
		 JLabel label2 = new JLabel ("Polinomul 2:");
		 JTextField tf2 = new JTextField("Introduceti polinomul2");
		 panel1.add(label2);
		 panel1.add(tf2);
		 frame1.add(panel1);
		 frame1.setVisible(true);
	}});
		 
		
		
b2.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent e) {
		 JFrame frame1= new JFrame("Scadere");
		 frame1.setSize(300,210);
		 JPanel panel1 = new JPanel();
		 JLabel label1 = new JLabel ("Polinomul 1:");
		 JTextField tf1 = new JTextField("Introduceti polinomul1");
		 panel1.add(label1);
		 panel1.add(tf1);
		 JLabel label2 = new JLabel ("Polinomul 2:");
		 JTextField tf2 = new JTextField("Introduceti polinomul2");
		 panel1.add(label2);
		 panel1.add(tf2);
		 frame1.add(panel1);
		 frame1.setVisible(true);
	}});
		 	
b3.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent e) {
		 JFrame frame1= new JFrame("Inmultire");
		 frame1.setSize(300,210);
		 JPanel panel1 = new JPanel();
		 JLabel label1 = new JLabel ("Polinomul 1:");
		 JTextField tf1 = new JTextField("Introduceti polinomul1");
		 panel1.add(label1);
		 panel1.add(tf1);
		 JLabel label2 = new JLabel ("Polinomul 2:");
		 JTextField tf2 = new JTextField("Introduceti polinomul2");
		 panel1.add(label2);
		 panel1.add(tf2);
		 frame1.add(panel1);
		 frame1.setVisible(true);
	}});
b4.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent e) {
		 JFrame frame1= new JFrame("Inmultire");
		 frame1.setSize(300,210);
		 JPanel panel1 = new JPanel();
		 JLabel label1 = new JLabel ("Polinomul 1:");
		 JTextField tf1 = new JTextField("Introduceti polinomul1");
		 panel1.add(label1);
		 panel1.add(tf1);
		 JLabel label2 = new JLabel ("Polinomul 2:");
		 JTextField tf2 = new JTextField("Introduceti polinomul2");
		 panel1.add(label2);
		 panel1.add(tf2);
		 frame1.add(panel1);
		 frame1.setVisible(true);
	}});
b5.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent e) {
		 JFrame frame1= new JFrame("Derivare");
		 frame1.setSize(300,210);
		 JPanel panel1 = new JPanel();
		 JLabel label1 = new JLabel ("Polinomul 1:");
		 JTextField tf1 = new JTextField("Introduceti polinomul1");
		 panel1.add(label1);
		 panel1.add(tf1);
		 JLabel label2 = new JLabel ("Polinomul 2:");
		 JTextField tf2 = new JTextField("Introduceti polinomul2");
		 panel1.add(label2);
		 panel1.add(tf2);
		 frame1.add(panel1);
		 frame1.setVisible(true);
	}});

b6.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent e) {
		 JFrame frame1= new JFrame("Integrare");
		 frame1.setSize(300,210);
		 JPanel panel1 = new JPanel();
		 JLabel label1 = new JLabel ("Polinomul 1:");
		 JTextField tf1 = new JTextField("Introduceti polinomul1");
		 panel1.add(label1);
		 panel1.add(tf1);
		 JLabel label2 = new JLabel ("Polinomul 2:");
		 JTextField tf2 = new JTextField("Introduceti polinomul2");
		 panel1.add(label2);
		 panel1.add(tf2);
		 frame1.add(panel1);
		 frame1.setVisible(true);
	}});

		 }}