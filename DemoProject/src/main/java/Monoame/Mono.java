package Monoame;
import Polinoame.Poli;
public class Mono {
	int  coeficient,exponent;
	String monom=new String();
	
	public Mono(String s)
	{
		this.monom=s;
	}
	public int getCoeficient() {
		return coeficient;
	}
	
	public int getExponent() {
		return exponent;
	}
	
	public String getMonom() {
		return monom;
	}
	public void setMonom(String monom) {
		this.monom = monom;
	}

	public String toString()
	{
		return monom;
	}
}
