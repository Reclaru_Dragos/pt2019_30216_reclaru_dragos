package Operatii;
import java.util.ArrayList;
import Polinoame.Poli;
public class Operatii {

	 private int exponent;
	private Object[] coeficient;

	
	public Operatii plus( Operatii b ){
		 Operatii a = this;
		 Operatii c = new Operatii( 0, Math.max( a.exponent, b.exponent ) );
	        for( int i = 0; i <= a.exponent; i++ ) 
	        	c.coeficient[ i ] =c.coeficient[ i ] + a.coeficient[ i ];
	        for( int i = 0; i <= b.exponent; i++ ) 
	        	c.coeficient[ i ] =c.coeficient[ i ] + b.coeficient[ i ];
	        c.exponent = c.exponent();
	        return c;
	}
}
